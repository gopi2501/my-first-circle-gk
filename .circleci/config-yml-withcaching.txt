version: 2
# we will be working on the cache in this configuration
jobs:
    build:
        docker:
            - image: circleci/node:4.8.2
        steps:
            - checkout
            #lets restore the cache which we have created below
            - restore_cache:
                key: npm-cache-{{ .Branch }}-{{ checksum "package.json" }}
            - run: npm install
            - save_cache: #npm-cache is the key for the cache you are saving
                key: npm-cache-{{ .Branch }}-{{ checksum "package.json" }}
                paths:
                    - node_modules